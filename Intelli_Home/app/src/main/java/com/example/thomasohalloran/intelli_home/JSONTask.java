package com.example.thomasohalloran.intelli_home;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.provider.ContactsContract;
import android.widget.Toast;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.json.JSONException;
import org.json.JSONObject;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import org.json.*;

public class JSONTask extends AsyncTask<String, String, String>{

    String error = "";
    private String type;
    private String numLight;
    private String actionType;
    private String jsonText;
    int postGet = 1;
    Context context;
    HttpURLConnection urlConnection = null;
    URL url = null;
    DataOutputStream dos = null;
    BufferedReader in = null;
    StringBuffer response = null;
    JSONObject jsonObj;

    public JSONTask(String name, String action, Context con)
    {
        type = name;
        actionType = action;
        context = con;
    }

    public JSONTask(String name, String action, String number, Context con)
    {
        type = name;
        actionType = action;
        numLight = number;
        context = con;
    }

    protected String doInBackground(String... params)
    {
        try
        {
            jsonObj = new JSONObject();
            jsonText = "";

            if(type.equalsIgnoreCase("profile"))
            {
                postGet = 0;
                url = new URL(params[0]);
                urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setDoInput(true);
                //urlConnection.setRequestMethod("GET");
                urlConnection.setRequestProperty("Content-Type", "application/json");
            }
            else if(type.equalsIgnoreCase("alarm"))
            {
                url = new URL(params[0]);
                urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setDoOutput(true); // Set Http method to POST
                urlConnection.setDoInput(true);
                urlConnection.setRequestMethod("POST");
                urlConnection.setRequestProperty("Content-Type", "application/json");
                dos = new DataOutputStream(urlConnection.getOutputStream());

                jsonObj.put("name", actionType);
                jsonText = jsonObj.toString();
            }
            else if (type.equalsIgnoreCase("heat"))
            {
                url = new URL(params[0]+"Heat");
                urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setDoOutput(true); // Set Http method to POST
                urlConnection.setDoInput(true);
                urlConnection.setRequestMethod("POST");
                urlConnection.setRequestProperty("Content-Type", "application/json");
                dos = new DataOutputStream(urlConnection.getOutputStream());

                jsonObj.put("name", actionType);
                jsonText = jsonObj.toString();
            }
            else if (type.equalsIgnoreCase("light"))
            {
                if(actionType.equalsIgnoreCase("On"))
                {
                    url = new URL(params[0]+"LightOn");
                }
                else if(actionType.equalsIgnoreCase("All"))
                {
                    url = new URL(params[0]+"LightAllOff");
                }
                else
                {
                    url = new URL(params[0]+"LightOff");
                }

                urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setDoOutput(true); // Set Http method to POST
                urlConnection.setDoInput(true);
                urlConnection.setRequestMethod("POST");
                urlConnection.setRequestProperty("Content-Type", "application/json");
                dos = new DataOutputStream(urlConnection.getOutputStream());

                jsonObj.put("name", numLight);
                jsonText = jsonObj.toString();
            }
            else if(type.equalsIgnoreCase("login"))
            {
                postGet = 2;
                //Add code to send post request to webserver to validate login
                url = new URL("http://node-62447.onmodulus.net/mobileLogin");  //what ever path is chosen for login

                urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setDoOutput(true); // Set Http method to POST
                urlConnection.setDoInput(true);
                urlConnection.setRequestMethod("POST");
                urlConnection.setRequestProperty("Content-Type", "application/json");
                dos = new DataOutputStream(urlConnection.getOutputStream());

                jsonObj.put("username", "001");
                jsonObj.put("password", "root");
                jsonText = jsonObj.toString();
            }
            else
            {
                Toast.makeText(this.context, "Unable to update Raspberry Pi", Toast.LENGTH_SHORT).show();
            }
            if( postGet == 1 || postGet == 2)
            {
                dos.writeBytes(jsonText);
                dos.flush();
                dos.close();
            }

            String inputLine;
            in = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
            response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
              response.append(inputLine);
            }
            in.close();
            return response.toString();
        }
        catch (MalformedURLException e) { e.printStackTrace(); }
        catch (IOException e) { e.printStackTrace(); }
        catch (JSONException e) { e.printStackTrace(); }
        return null;
    }

    @Override
    protected void onPostExecute(String result)
    {
        ProfileActivity.tempStatus.setText("" + ProfileActivity.heatTemp + "°C");
        JSONArray json = null;
        JSONObject jsonObj = null;
        super.onPostExecute(result);

        if(postGet == 0)
        {
            try
            {
                json = new JSONArray(result);

                jsonObj = new JSONObject(json.get(0).toString());
                ProfileActivity.alarm = Integer.parseInt(jsonObj.getString("alarm"));
                if( ProfileActivity.alarm == 1)
                {
                    ProfileActivity.alarmStatus.setText("On");
                }
                else
                {
                    ProfileActivity.alarmStatus.setText("Off");
                }

                jsonObj = new JSONObject(json.get(1).toString());
                ProfileActivity.heat = Integer.parseInt(jsonObj.getString("heating"));
                if( ProfileActivity.heat == 1)
                {
                    ProfileActivity.heatStatus.setText("On");
                }
                else
                {
                    ProfileActivity.heatStatus.setText("Off");
                }

                jsonObj = new JSONObject(json.get(2).toString());
                ProfileActivity.co2Alarm = Integer.parseInt(jsonObj.getString("CO2"));
                if( ProfileActivity.co2Alarm == 0)
                {
                    ProfileActivity.co2Status.setText("Safe");
                }
                else
                {
                    ProfileActivity.co2Status.setText("Un-Safe");
                }

                jsonObj = new JSONObject(json.get(3).toString());
                ProfileActivity.fireAlarm = Integer.parseInt(jsonObj.getString("Fire"));
                if( ProfileActivity.fireAlarm == 0)
                {
                    ProfileActivity.fireAlarmStatus.setText("Safe");
                }
                else
                {
                    ProfileActivity.fireAlarmStatus.setText("Un-Safe");
                }
//                jsonObj = new JSONObject(json.get(7).toString());
//                ProfileActivity.heatTemp = (jsonObj.getString("heatTemp"));


            }
            catch (JSONException e) { e.printStackTrace(); }
        }
        else if (postGet == 2)
        {
//            JSONObject jsonObject = null;
//            try {
//                jsonObject = new JSONObject(result);
//                Toast.makeText(this.context, jsonObject.getString("success"), Toast.LENGTH_SHORT).show();
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }
//            try
//            {
//                json = new JSONArray(result);
//
//                jsonObj = new JSONObject(json.get(0).toString());
//                LoginActivity.name = jsonObj.getString("name");
//
//                jsonObj = new JSONObject(json.get(1).toString());
//                LoginActivity.address = jsonObj.getString("address");
//
//                jsonObj = new JSONObject(json.get(2).toString());
//                LoginActivity.accountNumber = jsonObj.getString("accountNumber");
//
//                jsonObj = new JSONObject(json.get(3).toString());
//                LoginActivity.accountType = jsonObj.getString("accountType");
//
//                jsonObj = new JSONObject(json.get(4).toString());
//                LoginActivity.IP = jsonObj.getString("customerIP");
//            }
//            catch (JSONException e) { e.printStackTrace(); }
        }
        else
        {
            Toast.makeText(this.context, result, Toast.LENGTH_SHORT).show();
        }

    }
}