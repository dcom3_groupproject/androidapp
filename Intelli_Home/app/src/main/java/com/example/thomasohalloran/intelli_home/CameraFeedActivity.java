package com.example.thomasohalloran.intelli_home;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.WebView;

public class CameraFeedActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camerafeed);
        WebView webView = (WebView)findViewById(R.id.webView);
        // Set page content for webview
        webView.loadData("<html><head>" +
                        "<meta name='viewport' content='target-densitydpi=device-dpi,initial-scale=0.5,minimum-scale=0.1,user-scalable=yes'/>" +
                        "</head><body style=\"background:black;\"><center><img src=\"http://86.44.222.26:8081/\" alt=\"Stream\" align=\"middle\"></center></body></html>",
                        "text/html", null);
        webView.getSettings().setBuiltInZoomControls(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        menu.getItem(5).setVisible(ProfileActivity.showUpgrade);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        Intent intent;
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_profile)
        {
            intent = new Intent(this, ProfileActivity.class);
            startActivity(intent);
            finish();
            return true;
        }
        else if (id == R.id.action_heatlight)
        {
            intent = new Intent(this, LightHeatActivity.class);
            startActivity(intent);
            finish();
            return true;
        }
        else if (id == R.id.action_alarm)
        {
            intent = new Intent(this, AlarmActivity.class);
            startActivity(intent);
            finish();
            return true;
        }
        else if (id == R.id.action_cameraFeed)
        {
            intent = new Intent(this, CameraFeedActivity.class);
            startActivity(intent);
            finish();
            return true;
        }
        else if (id == R.id.action_twitFeed)
        {
            intent = new Intent(this, TwitterFeedActivity.class);
            startActivity(intent);
            finish();
            return true;
        }
        else if (id == R.id.action_upgrade) {
            intent = new Intent(this, UpgradeProActivity.class);
            startActivity(intent);
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    //create a pop up requesting user to confirm they wish to quit the app
    public void onBackPressed() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle(R.string.QuitDialogTitle);
        dialog.setMessage(R.string.QuitDialogMessage);

        dialog.setNegativeButton((getString(R.string.QuitDialogNegative)),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

        dialog.setPositiveButton((getString(R.string.QuitDialogPositive)),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        CameraFeedActivity.this.finish();
                    }
                });

        dialog.setIcon(R.mipmap.ic_launcher);
        dialog.show();
    }
}
