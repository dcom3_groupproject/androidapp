package com.example.thomasohalloran.intelli_home;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class ProfileActivity extends AppCompatActivity {

    //Create static variables
    static String accountNum, accountType, custIP, name, address;
    static int usrLogin = 0;
    static String url = "http://86.44.222.26:3458/", heatTemp = "";
    static int alarm = 0, heat = 0, co2Alarm = 0, fireAlarm = 0;
    static int light1 = 0, light2 = 0, light3 = 0;
    static TextView accountNumberStatus, accountTypeStatus, custName, custAddr;
    static TextView alarmStatus, co2Status, heatStatus, tempStatus, fireAlarmStatus;
    static boolean showUpgrade = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        //Only actioned once after initial user login
        if(usrLogin == 0)
        {
            Bundle bundle = getIntent().getExtras();
            name = bundle.getString("name");
            address = bundle.getString("address");
            accountNum = bundle.getString("acNum");
            accountType = bundle.getString("acType");
            custIP = bundle.getString("IP");
            usrLogin = 1;
        }

        custName = (TextView) findViewById(R.id.custName);
        custAddr = (TextView) findViewById(R.id.custAddr);
        accountNumberStatus = (TextView) findViewById(R.id.accountNumber);
        accountTypeStatus = (TextView) findViewById(R.id.accountType);
        alarmStatus = (TextView) findViewById(R.id.alarmStatus);
        heatStatus = (TextView) findViewById(R.id.heatStatus);
        tempStatus = (TextView) findViewById(R.id.tempDegree);
        co2Status = (TextView) findViewById(R.id.co2AlarmStatus);
        fireAlarmStatus = (TextView) findViewById(R.id.fireAlarmStatus);



        //new JSONTask("profile","Off", getApplicationContext()).execute(url);
        new GetJSON().execute();

        custName.setText(name);
        custAddr.setText(address);
        accountNumberStatus.setText(accountNum);
        if(showUpgrade)
        {
            accountType = "Standard";
        }
        else
        {
            accountType = "Full";
        }
        accountTypeStatus.setText(accountType);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        menu.getItem(5).setVisible(ProfileActivity.showUpgrade);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        Intent intent;
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_profile)
        {
            intent = new Intent(this, ProfileActivity.class);
            startActivity(intent);
            finish();
            return true;
        }
        else if (id == R.id.action_heatlight)
        {
            intent = new Intent(this, LightHeatActivity.class);
            startActivity(intent);
            finish();
            return true;
        }
        else if (id == R.id.action_alarm)
        {
            intent = new Intent(this, AlarmActivity.class);
            startActivity(intent);
            finish();
            return true;
        }
        else if (id == R.id.action_cameraFeed)
        {
            intent = new Intent(this, CameraFeedActivity.class);
            startActivity(intent);
            finish();
            return true;
        }
        else if (id == R.id.action_twitFeed)
        {
            intent = new Intent(this, TwitterFeedActivity.class);
            startActivity(intent);
            finish();
            return true;
        }
        else if (id == R.id.action_upgrade)
        {
            intent = new Intent(this, UpgradeProActivity.class);
            startActivity(intent);
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    //create a pop up requesting user to confirm they wish to quit the app
    public void onBackPressed() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle(R.string.QuitDialogTitle);
        dialog.setMessage(R.string.QuitDialogMessage);

        dialog.setNegativeButton((getString(R.string.QuitDialogNegative)),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

        dialog.setPositiveButton((getString(R.string.QuitDialogPositive)),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        ProfileActivity.this.finish();
                    }
                });

        dialog.setIcon(R.mipmap.ic_launcher);
        dialog.show();
    }

    class GetJSON extends AsyncTask<String, String, String> {

        private ProgressDialog progressDialog = new ProgressDialog(ProfileActivity.this);

        protected void onPreExecute() {
            progressDialog.setMessage("Downloading your data...");
            progressDialog.show();
            progressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                public void onCancel(DialogInterface arg0) {
                    GetJSON.this.cancel(true);
                }
            });
        }

        @Override
        protected String doInBackground(String... params) {
            String tempURL = "http://86.44.222.26:3457/";
            URL urlHeat = null;
            try {
                urlHeat = new URL(tempURL);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            HttpURLConnection request = null;
            try {
                request = (HttpURLConnection) urlHeat.openConnection();
                request.connect();
            } catch (IOException e) {
                e.printStackTrace();
            }

            JsonParser jp = new JsonParser(); //from gson
            JsonElement root = null; //Convert the input stream to a json element
            try {
                root = jp.parse(new InputStreamReader(request.getInputStream()));
            } catch (IOException e) {
                e.printStackTrace();
            }
            JsonObject rootObj = root.getAsJsonObject();
            ProfileActivity.heatTemp = rootObj.get("temperature").getAsString();

            HttpURLConnection urlConnection = null;
            StringBuffer response = null;
            BufferedReader in = null;
            try {
                URL piURL= new URL(url);
                urlConnection = (HttpURLConnection) piURL.openConnection();
                urlConnection.setDoInput(true);
                //urlConnection.setRequestMethod("GET");
                urlConnection.setRequestProperty("Content-Type", "application/json");
                String inputLine;
                in = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                response = new StringBuffer();

                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();
            }
            catch (IOException e) {
                e.printStackTrace();
            }

            return response.toString();
        }

        @Override
        protected void onPostExecute(String result) {
            ProfileActivity.tempStatus.setText("" + ProfileActivity.heatTemp + "°C");
            JSONArray json = null;
            JSONObject jsonObj = null;
            super.onPostExecute(result);
            try
               {
                   json = new JSONArray(result);

                   jsonObj = new JSONObject(json.get(0).toString());
                   ProfileActivity.alarm = Integer.parseInt(jsonObj.getString("alarm"));
                    if( ProfileActivity.alarm == 1)
                    {
                        ProfileActivity.alarmStatus.setText("On");
                    }
                    else
                    {
                        ProfileActivity.alarmStatus.setText("Off");
                    }

                    jsonObj = new JSONObject(json.get(1).toString());
                    ProfileActivity.heat = Integer.parseInt(jsonObj.getString("heating"));
                    if( ProfileActivity.heat == 1)
                    {
                        ProfileActivity.heatStatus.setText("On");
                    }
                    else
                    {
                        ProfileActivity.heatStatus.setText("Off");
                    }

                    jsonObj = new JSONObject(json.get(2).toString());
                    ProfileActivity.co2Alarm = Integer.parseInt(jsonObj.getString("CO2"));
                    if( ProfileActivity.co2Alarm == 0)
                    {
                        ProfileActivity.co2Status.setText("Safe");
                    }
                    else
                    {
                        ProfileActivity.co2Status.setText("Un-Safe");
                    }

                    jsonObj = new JSONObject(json.get(3).toString());
                    ProfileActivity.fireAlarm = Integer.parseInt(jsonObj.getString("Fire"));
                    if( ProfileActivity.fireAlarm == 0)
                    {
                        ProfileActivity.fireAlarmStatus.setText("Safe");
                    }
                    else
                    {
                        ProfileActivity.fireAlarmStatus.setText("Un-Safe");
                    }
//                jsonObj = new JSONObject(json.get(7).toString());
//                ProfileActivity.heatTemp = (jsonObj.getString("heatTemp"));
                }
                catch (JSONException e) { e.printStackTrace(); }
            this.progressDialog.dismiss();
        }
    }

}
