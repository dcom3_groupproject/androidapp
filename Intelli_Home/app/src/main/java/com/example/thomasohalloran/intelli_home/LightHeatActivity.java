package com.example.thomasohalloran.intelli_home;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class LightHeatActivity extends AppCompatActivity{

    TextView heatStatus, lightStatus, temp;
    Button lightOnBtn, lightOffBtn, lightAllOffBtn;
    Button heatOnBtn, heatOffBtn;
    Spinner lights;
    String numLight = "";
    //private String url;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lightheat);
        //url = "http://192.168.1.222:5000/";
        //new GetJSON().execute();

        temp = (TextView) findViewById(R.id.tempTextView);
        temp.setText(""+ProfileActivity.heatTemp+"°C");

        heatStatus = (TextView) findViewById(R.id.heatTextView);
        if(ProfileActivity.heat == 1)
        {
            heatStatus.setText("On");
        }
        else
        {
            heatStatus.setText("Off");
        }
        lightStatus = (TextView) findViewById(R.id.lightTextView);
        if(ProfileActivity.light1 == 1)
        {
            lightStatus.setText("On");
        }
        else
        {
            lightStatus.setText("Off");
        }

        heatOnBtn = (Button) findViewById(R.id.heatBtnOn);
        heatOffBtn = (Button) findViewById(R.id.heatBtnOff);
        lightOnBtn = (Button) findViewById(R.id.lightBtnOn);
        lightOffBtn = (Button) findViewById(R.id.lightBtnOff);
        lightAllOffBtn = (Button) findViewById(R.id.lightBtnAllOff);

        lights = (Spinner) findViewById(R.id.lightsList);
        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.lights_array,
                                                                                android.R.layout.simple_spinner_item);
        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        lights.setAdapter(adapter);
        lights.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
        {
            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1, int position, long id) {
                // TODO Auto-generated method stub
                numLight = lights.getSelectedItem().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub
            }
        });

        heatOnBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new JSONTask("heat", "On", getApplicationContext()).execute(ProfileActivity.url);
                heatStatus.setText("On");
            }
        });

        heatOffBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new JSONTask("heat", "Off", getApplicationContext()).execute(ProfileActivity.url);
                heatStatus.setText("Off");
            }
        });

        lightOnBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new JSONTask("light", "On", numLight, getApplicationContext()).execute(ProfileActivity.url);
                lightStatus.setText("On");
            }
        });

        lightOffBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new JSONTask("light","Off", numLight, getApplicationContext()).execute(ProfileActivity.url);
                lightStatus.setText("Off");
            }
        });

        lightAllOffBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new JSONTask("light", "All","1", getApplicationContext()).execute(ProfileActivity.url);
                lightStatus.setText("Off");
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        menu.getItem(5).setVisible(ProfileActivity.showUpgrade);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        Intent intent;
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_profile) {
            intent = new Intent(this, ProfileActivity.class);
            startActivity(intent);
            finish();
            return true;
        } else if (id == R.id.action_heatlight) {
            intent = new Intent(this, LightHeatActivity.class);
            startActivity(intent);
            finish();
            return true;
        } else if (id == R.id.action_alarm) {
            intent = new Intent(this, AlarmActivity.class);
            startActivity(intent);
            finish();
            return true;
        } else if (id == R.id.action_cameraFeed) {
            intent = new Intent(this, CameraFeedActivity.class);
            startActivity(intent);
            finish();
            return true;
        } else if (id == R.id.action_twitFeed) {
            intent = new Intent(this, TwitterFeedActivity.class);
            startActivity(intent);
            finish();
            return true;
        } else if (id == R.id.action_upgrade) {
            intent = new Intent(this, UpgradeProActivity.class);
            startActivity(intent);
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    //create a pop up requesting user to confirm they wish to quit the app
    public void onBackPressed() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle(R.string.QuitDialogTitle);
        dialog.setMessage(R.string.QuitDialogMessage);

        dialog.setNegativeButton((getString(R.string.QuitDialogNegative)),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

        dialog.setPositiveButton((getString(R.string.QuitDialogPositive)),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        LightHeatActivity.this.finish();
                    }
                });

        dialog.setIcon(R.mipmap.ic_launcher);
        dialog.show();
    }
}