package com.example.thomasohalloran.intelli_home;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;

import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterCore;
import com.twitter.sdk.android.tweetui.TweetTimelineListAdapter;
import com.twitter.sdk.android.tweetui.UserTimeline;

import io.fabric.sdk.android.Fabric;

public class TwitterFeedActivity extends AppCompatActivity {

    private static final String TWITTER_KEY = "XxhcsCdVWjbifupuXu17prwMg";
    private static final String TWITTER_SECRET = "bu2TLZCYkMVomJlBNRiiBae5VJ5agFZY9x790wHm9LaFRpzDhW";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_twitterfeed);

        // Note: Your consumer key and secret should be obfuscated in your source code before shipping

        TwitterAuthConfig authConfig = new TwitterAuthConfig(TWITTER_KEY, TWITTER_SECRET);
        Fabric.with(this, new Twitter(authConfig));
//
//        TwitterAuthConfig authConfig =  new TwitterAuthConfig("consumerKey", "consumerSecret");
//        Fabric.with(this, new TwitterCore(authConfig));

        ListView listView = (ListView) findViewById(R.id.listView);

        final UserTimeline userTimeline = new UserTimeline.Builder()
                .screenName("Intelli_home")
                .build();
        final TweetTimelineListAdapter adapter = new TweetTimelineListAdapter.Builder(this)
                .setTimeline(userTimeline)
                .build();
        listView.setAdapter(adapter);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        menu.getItem(5).setVisible(ProfileActivity.showUpgrade);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        Intent intent;
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_profile)
        {
            intent = new Intent(this, ProfileActivity.class);
            startActivity(intent);
            finish();
            return true;
        }
        else if (id == R.id.action_heatlight)
        {
            intent = new Intent(this, LightHeatActivity.class);
            startActivity(intent);
            finish();
            return true;
        }
        else if (id == R.id.action_alarm)
        {
            intent = new Intent(this, AlarmActivity.class);
            startActivity(intent);
            finish();
            return true;
        }
        else if (id == R.id.action_cameraFeed)
        {
            intent = new Intent(this, CameraFeedActivity.class);
            startActivity(intent);
            finish();
            return true;
        }
        else if (id == R.id.action_twitFeed)
        {
            intent = new Intent(this, TwitterFeedActivity.class);
            startActivity(intent);
            finish();
            return true;
        }
        else if (id == R.id.action_upgrade) {
            intent = new Intent(this, UpgradeProActivity.class);
            startActivity(intent);
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    //create a pop up requesting user to confirm they wish to quit the app
    public void onBackPressed() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle(R.string.QuitDialogTitle);
        dialog.setMessage(R.string.QuitDialogMessage);

        dialog.setNegativeButton((getString(R.string.QuitDialogNegative)),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

        dialog.setPositiveButton((getString(R.string.QuitDialogPositive)),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        TwitterFeedActivity.this.finish();
                    }
                });

        dialog.setIcon(R.mipmap.ic_launcher);
        dialog.show();
    }

}
