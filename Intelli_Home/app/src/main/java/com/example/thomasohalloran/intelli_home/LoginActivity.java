package com.example.thomasohalloran.intelli_home;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import io.fabric.sdk.android.Fabric;

public class LoginActivity extends AppCompatActivity {

    static String usrname, passwd, IP, accountNumber, accountType, name, address;
    EditText usrnameET, passwdET;
    Button loginBtn, resetBtn;
    Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        loginBtn = (Button) findViewById(R.id.loginBtn);
        resetBtn = (Button) findViewById(R.id.resetBtn);
        usrnameET = (EditText) findViewById(R.id.userNameID);
        passwdET = (EditText) findViewById(R.id.userPwdID);

        //Testing purposes
        IP = "1234";
        name = "Sir Dave Kavanagh";
        address = "Ballincollig, Cork";
        accountNumber = "IH003214214";
        accountType = "Standard";

        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //Add validation to make sure u+p are not empty
                if(usrnameET.getText().toString().isEmpty() || passwdET.getText().toString().isEmpty())
                {
                    /*
                    Replace toast below with notification of blank fields
                     */
                    Toast.makeText(LoginActivity.this, "Login error, please complete both fields!!", Toast.LENGTH_SHORT).show();
                    usrnameET.setText("");
                    passwdET.setText("");
                }
                else
                {
                    usrname = usrnameET.getText().toString();
                    passwd = passwdET.getText().toString();

                    //Following will only be used when webserver is running to verify user login
                    new JSONTask("login", "login", getApplicationContext()).execute("http://node-62447.onmodulus.net/mobileLogin"); // Addr of webserver

                    intent = new Intent(LoginActivity.this, ProfileActivity.class);
                    intent.putExtra("IP", IP);
                    intent.putExtra("acNum", accountNumber);
                    intent.putExtra("acType", accountType);
                    intent.putExtra("name", name);
                    intent.putExtra("address", address);

                    startActivity(intent);
                    finish();
                }
            }
        });

        resetBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                    usrnameET.setText("");
                    passwdET.setText("");
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_profile)
        {
            intent = new Intent(this, ProfileActivity.class);
            startActivity(intent);
            finish();
            return true;
        }
        else if (id == R.id.action_heatlight)
        {
            intent = new Intent(this, LightHeatActivity.class);
            startActivity(intent);
            finish();
            return true;
        }
        else if (id == R.id.action_alarm)
        {
            intent = new Intent(this, AlarmActivity.class);
            startActivity(intent);
            finish();
            return true;
        }
        else if (id == R.id.action_cameraFeed)
        {
            intent = new Intent(this, CameraFeedActivity.class);
            startActivity(intent);
            finish();
            return true;
        }
        else if (id == R.id.action_twitFeed)
        {
            intent = new Intent(this, TwitterFeedActivity.class);
            startActivity(intent);
            finish();
            return true;
        }
        else if (id == R.id.action_upgrade)
        {
            intent = new Intent(this, UpgradeProActivity.class);
            startActivity(intent);
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
