package com.example.thomasohalloran.intelli_home;

import android.app.PendingIntent;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.TextView;

public class AlarmActivity extends AppCompatActivity {

    Button alarmBtnOn, alarmBtnOff;
    Button contactESBtn, contactSHBtn;
    TextView alarmStatus, co2Status, fireStatus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alarm);

        alarmStatus = (TextView) findViewById(R.id.alarmTextView);
        if(ProfileActivity.alarm == 1)
        {
            alarmStatus.setText("On");
        }
        else
        {
            alarmStatus.setText("Off");
        }
        co2Status = (TextView) findViewById(R.id.co2AlarmTextView);
        if(ProfileActivity.co2Alarm == 0)
        {
            co2Status.setText("Safe");
        }
        else
        {
            co2Status.setText("Warning!!!");
        }
        fireStatus = (TextView) findViewById(R.id.fireAlarmTextView);
        if(ProfileActivity.fireAlarm == 0)
        {
            fireStatus.setText("Safe");
        }
        else
        {
            fireStatus.setText("Warning!!!");
        }

        alarmBtnOn = (Button) findViewById(R.id.alarmBtnOn);
        alarmBtnOff = (Button) findViewById(R.id.alarmBtnOff);
        contactESBtn = (Button) findViewById(R.id.contactESBtn);
        contactSHBtn = (Button) findViewById(R.id.contactSecuriBtn);

        alarmBtnOn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new JSONTask("alarm","On", getApplicationContext()).execute(ProfileActivity.url);
                alarmStatus.setText("On");
            }
        });

        alarmBtnOff.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new JSONTask("alarm","Off", getApplicationContext()).execute(ProfileActivity.url);
                alarmStatus.setText("Off");
            }
        });

        contactESBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:999"));
                try {
                    AlarmActivity.this.startActivity(callIntent);
                }catch(SecurityException s) {
                    s.printStackTrace();
                }
            }
        });

        contactSHBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:18001234567"));
                try {
                    AlarmActivity.this.startActivity(callIntent);
                }catch(SecurityException s) {
                    s.printStackTrace();
                }
            }
        });


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        menu.getItem(5).setVisible(ProfileActivity.showUpgrade);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        Intent intent;
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_profile)
        {
            intent = new Intent(this, ProfileActivity.class);
            startActivity(intent);
            finish();
            return true;
        }
        else if (id == R.id.action_heatlight)
        {
            intent = new Intent(this, LightHeatActivity.class);
            startActivity(intent);
            finish();
            return true;
        }
        else if (id == R.id.action_alarm)
        {
            intent = new Intent(this, AlarmActivity.class);
            startActivity(intent);
            finish();
            return true;
        }
        else if (id == R.id.action_cameraFeed)
        {
            intent = new Intent(this, CameraFeedActivity.class);
            startActivity(intent);
            finish();
            return true;
        }
        else if (id == R.id.action_twitFeed)
        {
            intent = new Intent(this, TwitterFeedActivity.class);
            startActivity(intent);
            finish();
            return true;
        }
        else if (id == R.id.action_upgrade)
        {
            intent = new Intent(this, UpgradeProActivity.class);
            startActivity(intent);
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    //create a pop up requesting user to confirm they wish to quit the app
    public void onBackPressed() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle(R.string.QuitDialogTitle);
        dialog.setMessage(R.string.QuitDialogMessage);

        dialog.setNegativeButton((getString(R.string.QuitDialogNegative)),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

        dialog.setPositiveButton((getString(R.string.QuitDialogPositive)),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        AlarmActivity.this.finish();
                    }
                });

        dialog.setIcon(R.mipmap.ic_launcher);
        dialog.show();
    }

}
